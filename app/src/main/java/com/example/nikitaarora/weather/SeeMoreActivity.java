package com.example.nikitaarora.weather;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SeeMoreActivity extends AppCompatActivity {
    JSONObject jObject;
    JSONArray totalArray;

    NextContent content;
    public ArrayList<NextContent> arrayContent = new ArrayList<>();

    ListView list;
    TextView list_txt = (TextView) findViewById(R.id.textView_list);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_more);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void getResponse(View view) {
        String url = "";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("response", response.toString());
                        try {
                            totalArray = response.getJSONArray("list");
                            Log.d("total", String.valueOf(totalArray));

                            for (int j =0; j<totalArray.length(); j++)
                            {
                                jObject = totalArray.getJSONObject(j);
                                content = new NextContent(jObject);
                                arrayContent.add(content);
                            }

                            ArrayAdapter<String> adapt = new ArrayAdapter<String>(
                                    getBaseContext(),
                                    R.layout.see_more_list_item,
                                    R.id.textView_list,
                                    (List<String>) list);

                            ListView listView = (ListView) findViewById(R.id.lisView_more);
                            listView.setAdapter(adapt);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("response", "error in response message");
                        error.printStackTrace();
                    }

                });
        requestQueue.add(jsonObjectRequest);

    }

}
