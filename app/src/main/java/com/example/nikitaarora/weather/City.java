package com.example.nikitaarora.weather;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import java.util.List;

/**
 * Created by nikita.arora on 11/2/2015.
 */
public class City implements ParentObject {

    /* an instance variable for the list of children */
    private List<Object> mChildrenList;

    @Override
    public List<Object> getChildObjectList() {
        return mChildrenList;
    }

    @Override
    public void setChildObjectList(List<Object> list) {
        mChildrenList = list;
    }
}