package com.example.nikitaarora.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import java.util.List;

/**
 * Created by nikita.arora on 11/2/2015.
 */
public class CustomAdapter extends ExpandableRecyclerAdapter<CityParentViewHolder, TempChildViewHolder> {
    LayoutInflater mInflater;

    public CustomAdapter(Context context, List<ParentObject> parentItemList) {
        super(context, parentItemList);
    }

    @Override
    public CityParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.parent_row_item, viewGroup, false);
        return new CityParentViewHolder(view);
    }

    @Override
    public TempChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.child_row_item, viewGroup, false);
        return new TempChildViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(CityParentViewHolder parentHolder, int i, Object parentObject) {
        Parent parent = (Parent) parentObject;
        parentHolder.parentCityName.setText(parent.getCity_name());
        parentHolder.parentTemp.setText(parent.getTemp());

    }

    @Override
    public void onBindChildViewHolder(TempChildViewHolder tempChildViewHolder, int i, Object childObject) {
        Child child = (Child) childObject;
        tempChildViewHolder.childDesc.setText(child.getDesc());
        tempChildViewHolder.childMin.setText(child.getTempMin());
        tempChildViewHolder.childMax.setText(child.getTempMax());
    }
}
