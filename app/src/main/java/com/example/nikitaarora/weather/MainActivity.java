package com.example.nikitaarora.weather;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private CustomAdapter mAdapter;

    JSONObject jObject;
    JSONArray totalArray;
    Parent parent;
    Child child;
    public ArrayList<Parent> arrayParent = new ArrayList<>();
    public ArrayList<Child> arrayChild = new ArrayList<>();
    public List<City> city;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycle_UI);
        manager = new LinearLayoutManager(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getResponse(View view) {
        String url = "http://api.openweathermap.org/data/2.5/group?id=1275339,1277333,1273294,1269517,1273865,1269743,1274746,1278710,1254808,1264728&units=metric&appid=bd82977b86bf27fb59a04b61b657fb6f";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("response", response.toString());
                        try {
                            totalArray = response.getJSONArray("list");
                            Log.d("total", String.valueOf(totalArray));


                            for (int j =0; j<totalArray.length(); j++)
                            {

                                jObject = totalArray.getJSONObject(j);
                                parent = new Parent(jObject);
                                arrayParent.add(parent);
                                child = new Child(jObject);
                                arrayChild.add(child);
                            }

                            mAdapter = new CustomAdapter(getBaseContext(), generateCities());
                            mAdapter.setCustomParentAnimationViewId(R.id.arrow_parent);
                            mAdapter.setParentClickableViewAnimationDefaultDuration();
                            mAdapter.setParentAndIconExpandOnClick(true);

                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(mAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("response", "error in response message");
                        error.printStackTrace();
                    }

                });
        requestQueue.add(jsonObjectRequest);

    }


    private ArrayList<ParentObject> generateCities() {
        List<City> cities = new ArrayList<Parent>();
         cities = arrayParent;
        Child child = new Child(jObject);
        ArrayList<ParentObject> parentObjects = new ArrayList<>();
            for (City city : cities) {
            ArrayList<Object> childList = new ArrayList<>();
            childList.add(new Child(child.getDesc(), child.getTempMin(), child.getTempMin()));
            city.setChildObjectList(childList);
            parentObjects.add(city);
        }
        return parentObjects;
    }

}
