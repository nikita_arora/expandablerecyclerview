package com.example.nikitaarora.weather;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by nikita.arora on 11/2/2015.
 */
public class Parent {
    private String city, temp, id;
    private JSONObject contentDataObject;

    public Parent(JSONObject object) {

        this.contentDataObject = object;
        setCity_name();
        setTemp();
        setCity_id();
        this.city = getCity_name();
        this.temp = getTemp();
        this.id = getId();
    }

    public void setCity_name() {
        try {
            this.city = contentDataObject.optString("name");
            Log.d("city-name", city);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTemp() {
        try {
            this.temp = contentDataObject.getJSONObject("main").optString("temp");
            Log.d("city-temp", temp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCity_id() {
        try {
            this.city = contentDataObject.optString("id");
            Log.d("city-id", city);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getCity_name() {
        return this.city;
    }

    public String getTemp() {
        return this.temp;
    }

    public String getId() {return this.id;  }





}
