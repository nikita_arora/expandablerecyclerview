package com.example.nikitaarora.weather;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

/**
 * Created by nikita.arora on 11/1/2015.
 */
public class TempChildViewHolder extends ChildViewHolder implements OnItemClickListener{

    private OnItemClickListener mListener;
    int position;

    public TextView childDesc, childMin, childMax;
    public Button more;

    public TempChildViewHolder(View itemView) {
        super(itemView);
        childDesc = (TextView) itemView.findViewById(R.id.desc_child);
        childMin = (TextView) itemView.findViewById(R.id.min_child);
        childMax = (TextView) itemView.findViewById(R.id.max_child);
        more = (Button) itemView.findViewById(R.id.more_button);
        more.setOnClickListener((View.OnClickListener) this);
    }

    public TempChildViewHolder(View itemView, OnItemClickListener listener) {
        this(itemView);
        mListener = listener;
    }


    @Override
    public void onItemClick(View view, int position) {

        Intent i = new Intent(childDesc.getContext(), SeeMoreActivity.class);

    }
}
