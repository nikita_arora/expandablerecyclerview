package com.example.nikitaarora.weather;

import android.view.View;

/**
 * Created by nikita.arora on 11/2/2015.
 */
public interface OnItemClickListener {
    public void onItemClick(View view, int position);
}
