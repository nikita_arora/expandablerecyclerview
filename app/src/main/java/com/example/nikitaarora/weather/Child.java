package com.example.nikitaarora.weather;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by nikita.arora on 11/2/2015.
 */
public class Child {
    private String desc, tempMin, tempMax;
    private JSONObject contentDataObject;

    public Child(JSONObject object) {

        this.contentDataObject = object;
        setDesc();
        setTemp_Min();
        setTemp_Max();
        this.desc = getDesc();
        this.tempMin = getTempMin();
        this.tempMax = getTempMax();
    }


    public Child(String desc, String tempMin, String tempMax) {
        this.desc = desc;
        this.tempMin = tempMin;
        this.tempMax = tempMax;
    }

    public void setDesc() {
        try {
            this.desc = contentDataObject.getJSONArray("weather").getJSONObject(0).optString("description");
            Log.d("desc", desc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTemp_Min() {
        try {
            this.tempMin = contentDataObject.getJSONObject("main").optString("temp_min");
            Log.d("temp-min", tempMin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTemp_Max() {
        try {
            this.tempMax = contentDataObject.getJSONObject("main").optString("temp_max");
            Log.d("temp-max", tempMax);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDesc() {
        return this.desc;
    }

    public String getTempMin() {
        return this.tempMin;
    }

    public String getTempMax() {
        return this.tempMax;
    }
}
