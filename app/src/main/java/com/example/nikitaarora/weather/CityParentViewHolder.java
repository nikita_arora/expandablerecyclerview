package com.example.nikitaarora.weather;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

/**
 * Created by nikita.arora on 11/1/2015.
 */
public class CityParentViewHolder extends ParentViewHolder {
    public TextView parentCityName, parentTemp;
    public ImageView parentArrow;

    public CityParentViewHolder(View itemView) {
        super(itemView);

        parentCityName = (TextView) itemView.findViewById(R.id.txtView_parent);
        parentTemp = (TextView) itemView.findViewById(R.id.temp_parent);
        parentArrow = (ImageView) itemView.findViewById(R.id.arrow_parent);
    }
}
