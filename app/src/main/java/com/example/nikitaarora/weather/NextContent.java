package com.example.nikitaarora.weather;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by nikita.arora on 11/2/2015.
 */
public class NextContent {

    private String name, sunrise, sunset, pressure, humidity;
    private JSONObject contentDataObject;

    public NextContent(JSONObject object) {

        this.contentDataObject = object;
        setCity_name();
        setSunrise();
        setSunset();
        setPressure();
        setHumidity();
        this.name = getName();
        this.sunrise = getSunrise();
        this.sunset = getSunset();
        this.pressure = getPressure();
        this.humidity = getHumidity();
    }

    public void setCity_name() {
        try {
            this.name = contentDataObject.optString("name");
            Log.d("city-name", name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSunrise() {
        try {
            this.sunrise = contentDataObject.getJSONObject("sys").optString("sunrise");
            Log.d("sunrise", sunrise);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSunset() {
        try {
            this.sunset = contentDataObject.getJSONObject("sys").optString("sunset");
            Log.d("sunset", sunset);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setPressure() {
        try {
            this.pressure = contentDataObject.getJSONObject("main").optString("pressure");
            Log.d("pressure", pressure);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setHumidity() {
        try {
            this.humidity = contentDataObject.getJSONObject("main").optString("humidity");
            Log.d("humidity", humidity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return this.name;
    }

    public String getSunrise() {
        return this.sunrise;
    }

    public String getSunset() {
        return this.sunset;
    }

    public String getPressure() {
        return this.pressure;
    }

    public String getHumidity() {
        return this.humidity;
    }
}
